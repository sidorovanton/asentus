<?php
$MESS['MANAO_ASENTUS_INSTALL_DESCRIPTION'] = "Мастер создания сайта на базе шаблона Asentus";
$MESS['MANAO_ASENTUS_MODULE_INSTALLING'] = "Установка модуля";
$MESS['MANAO_ASENTUS_MODULE_UNINSTALLING'] = "Деинсталляция модуля";
$MESS['MANAO_ASENTUS_INSTALL_NAME'] = "Сайт на базе шаблона Asentus";
$MESS['MANAO_PARTNER'] = "Manao";
$MESS['PARTNER_URI'] = "http://manao.by";
