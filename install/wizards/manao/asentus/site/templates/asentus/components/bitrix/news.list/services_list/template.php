<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="bg-color-sky-light" data-auto-height="true">
    <div class="content-lg container">
        <div class="row row-space-1 margin-b-2">
            <? $count = 1; ?>
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <? if ($count % 3 == 1 && $count > 3): ?>
                    <div class="row row-space-1 margin-b-2">
                <? endif; ?>
                <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                     class="col-sm-4 <? if ($count % 3 != 0): ?> sm-margin-b-2<? endif; ?>">
                    <div class="wow fadeInLeft animated" data-wow-duration=".3" data-wow-delay=".3s"
                         style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                        <div class="service" data-height="height" style="height: 303px;">
                            <div class="service-element">
                                <? if (is_array($arItem["DISPLAY_PROPERTIES"]["SERVICES_ICON"])): ?>
                                    <i class="service-icon <?= $arItem["DISPLAY_PROPERTIES"]["SERVICES_ICON"]["VALUE"] ?>"></i>
                                <? endif; ?>
                            </div>
                            <div class="service-info">
                                <? if ($arItem["NAME"]): ?>
                                    <h3><?= $arItem["NAME"] ?></h3>
                                <? endif; ?>
                                <? if ($arItem["PREVIEW_TEXT"]): ?>
                                    <p class="margin-b-5"><?= $arItem["PREVIEW_TEXT"]; ?></p>
                                <? endif; ?>
                            </div>
                            <? if ($arItem["DETAIL_PAGE_URL"]): ?>
                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="content-wrapper-link"></a>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
                <? if ($count % 3 == 0 && $count != 1): ?>
                    </div>
                <? endif; ?>
                <? $count++; ?>
            <? endforeach; ?>
        </div>
    </div>
