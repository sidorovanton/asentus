<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetAdditionalCss(SITE_TEMPLATE_PATH . "/css/accordion.css"); ?>

<div class="bg-color-sky-light" data-auto-height="true">
    <div class="content-lg container">
        <div class="row row-space-2 margin-b-2">
            <? $count = 1; ?>
            <? foreach ($arResult["ITEMS"] as $arItem):
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
                <? if ($count % 2 == 1 && $count > 2): ?>
                    <div class="row row-space-2 <? if ($count % 2 == 0) echo 'margin-b-2' ?>">
                <? endif; ?>
                <div class="col-sm-6 <? if ($count % 2 == 1) echo 'sm-margin-b-2' ?>">
                    <div class="wow fadeInLeft animated" data-wow-duration=".3" data-wow-delay=".2s">
                        <div class="service">
                            <input class="tab-input" id="tab-<?= $arItem['ID'] ?>" type="radio" name="tabs">
                            <label class="content-wrapper-link tab-label" for="tab-<?= $arItem['ID'] ?>"
                                   id="<?= $this->GetEditAreaId($arItem['ID']); ?>"></label>
                            <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                                <h3><?= $arItem["NAME"] ?></h3>
                            <? endif; ?>
                            <div class="tab-content">
                                <? if ($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]): ?>
                                    <p class="margin-b-5"><?= $arItem["PREVIEW_TEXT"]; ?></p>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <? if ($count % 2 == 0 && $count != 1): ?>
                    </div>
                <? endif; ?>
                <? $count++; ?>
            <? endforeach; ?>
        </div>
    </div>
</div>