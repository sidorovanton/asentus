<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="section-seperator" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <div class="content-lg container">
            <div class="row margin-b-20">
                <div class="col-sm-6">
                    <a id="<?= $arItem["ID"] ?>"></a>
                    <? if ($arItem["NAME"]): ?>
                        <h2><?= $arItem["NAME"] ?></h2>
                    <? endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-7 sm-margin-b-50">
                    <? if (strlen($arItem["DETAIL_TEXT"]) > 0): ?>
                        <p><?= $arItem["DETAIL_TEXT"] ?></p>
                    <? else: ?>
                        <p><?= $arItem["PREVIEW_TEXT"] ?></p>
                    <? endif; ?>
                </div>
                <div class="col-sm-4 col-sm-offset-1">
                    <? if (is_array($arItem["DETAIL_PICTURE"])): ?>
                        <img
                                class="img-responsive"
                                src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>"
                                width="<?= $arItem["DETAIL_PICTURE"]["WIDTH"] ?>"
                                height="<?= $arItem["DETAIL_PICTURE"]["HEIGHT"] ?>"
                                alt="<?= $arItem["DETAIL_PICTURE"]["ALT"] ?>"
                                title="<?= $arItem["DETAIL_PICTURE"]["TITLE"] ?>"
                        />
                    <? elseif (is_array($arItem["PREVIEW_PICTURE"])): ?>
                        <img
                                class="img-responsive"
                                src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>"
                                height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>"
                                alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                        />
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
<? endforeach; ?>

