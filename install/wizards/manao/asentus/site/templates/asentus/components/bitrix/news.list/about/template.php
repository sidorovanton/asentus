<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<div class="section-seperator">
    <div class="content-lg container">
        <div class="row">
            <? $count = 1; ?>
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <? if ($count % 3 == 1 && $count > 3): ?>
                    <div class="row" style="margin-top:50px">
                <? endif; ?>
                <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                     class="col-sm-4 <? if ($count % 3 != 0): ?> sm-margin-b-50<? endif; ?>">
                    <div class="wow fadeInLeft animated" data-wow-duration=".3" data-wow-delay=".3s"
                         style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                        <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                            <h3><?= $arItem["NAME"] ?></h3>
                        <? endif; ?>
                        <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                            <p><?= $arItem["PREVIEW_TEXT"]; ?></p>
                        <? endif; ?>
                    </div>
                </div>
                <? if ($count % 3 == 0 && $count != 1): ?>
                    </div>
                <? endif; ?>
                <? $count++; ?>
            <? endforeach; ?>
        </div>
    </div>
