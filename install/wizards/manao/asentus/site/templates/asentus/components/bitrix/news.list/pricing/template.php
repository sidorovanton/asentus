<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="bg-color-sky-light">
    <div class="content-lg container">
        <div class="row row-space-1">
            <? $count = 1;
            $properties = array();
            foreach ($arResult["ITEMS"] as $arItem):
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
                <? if ($count % 3 == 1 && $count > 3): ?>
                    <div class="row row-space-1">
                <? endif; ?>
                <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                     class="col-sm-4 <? if ($count % 3 != 0): ?> sm-margin-b-2<? endif; ?>">
                    <div class="wow fadeInLeft animated" data-wow-duration=".3" data-wow-delay=".1s"
                         style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">
                        <div class="pricing">
                            <div class="margin-b-30">
                                <? if ($arItem["DISPLAY_PROPERTIES"]["ICON"]): ?>
                                    <i class="pricing-icon <?= $arItem["DISPLAY_PROPERTIES"]["ICON"]["VALUE"] ?>"></i>
                                <? endif; ?>
                                <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                                    <h3><?= $arItem["NAME"] ?>
                                        <? if ($arItem["DISPLAY_PROPERTIES"]["CURRENCY"]): ?>
                                            <span> - <?= $arItem["DISPLAY_PROPERTIES"]["CURRENCY"]["VALUE"] ?></span>
                                        <? endif; ?>
                                        <? if ($arItem["DISPLAY_PROPERTIES"]["PRICE"]): ?>
                                            <?= $arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"] ?>
                                        <? endif; ?>
                                    </h3>
                                <? endif; ?>
                                <? if ($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]): ?>
                                    <p><?= $arItem["PREVIEW_TEXT"]; ?></p>
                                <? endif; ?>
                            </div>
                            <ul class="list-unstyled pricing-list margin-b-50">
                                <? if ($arItem["DISPLAY_PROPERTIES"]["FEATURES"]): ?>
                                    <li class="pricing-list-item"><?= $arItem["DISPLAY_PROPERTIES"]["FEATURES"]["VALUE"] ?></li>
                                <? endif; ?>
                                <? if ($arItem["DISPLAY_PROPERTIES"]["PRODUCTS"]): ?>
                                    <li class="pricing-list-item"><?= $arItem["DISPLAY_PROPERTIES"]["PRODUCTS"]["VALUE"] ?></li>
                                <? endif; ?>
                                <? if ($arItem["DISPLAY_PROPERTIES"]["PANELS"]): ?>
                                    <li class="pricing-list-item"><?= $arItem["DISPLAY_PROPERTIES"]["PANELS"]["VALUE"] ?></li>
                                <? endif; ?>
                            </ul>
                            <a href="javascript:void(0)" id="<?= $arItem['ID'] ?>"
                               class="btn-popup-pricing btn-theme btn-theme-sm btn-default-bg text-uppercase"><?= GetMessage("MFT_PRICING_BUTTON") ?></a>
                        </div>
                    </div>
                </div>

                <? if ($count % 3 == 0 && $count != 1): ?>
                    </div>
                <? endif; ?>
                <? $count++; ?>
                <? $properties[$arItem["ID"]] = $arItem['NAME']; ?>
            <? endforeach; ?>
        </div>
    </div>

    <script>
        BX.ready(function () {
            BX.bindDelegate(document.body, 'click', {className: 'btn-popup-pricing'}, function (e) {
                var d = JSON.parse('<?=json_encode($properties)?>');
                console.log(d);
                BX.ajax({
                    url: '<?=SITE_TEMPLATE_PATH?>/ajax/feedback.php',
                    data: {
                        ajax: 'Y',
                        data: d,
                        index: e.target.id
                    },
                    method: 'POST',
                    onsuccess: function (data) {
                        var t = new BX.PopupWindow(
                            "pricing_popup",
                            null,
                            {
                                content: data,
                                closeIcon: {right: "20px", top: "10px"},
                                titleBar: {content: ''},
                                zIndex: 0,
                                offsetLeft: 0,
                                offsetTop: 0,
                                draggable: {restrict: false}
                            });
                        t.show();
                    }
                });
            });
        });
    </script>



