<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="row margin-b-50">
    <? $count = 1;
    foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <? if ($count % 3 == 1 && $count > 3): ?>
            <div class="row margin-b-50">
        <? endif; ?>
        <div class="col-sm-4 sm-margin-b-50" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="margin-b-20">
                <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                    <div class="wow zoomIn" data-wow-duration=".3" data-wow-delay=".1s">
                        <img
                            class="img-responsive"
                            src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                            width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>"
                            height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>"
                            alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                            title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                        />
                    </div>
                <? endif; ?>
                <? if ($arParams["DISPLAY_PICTURE"] != "N" && !is_array($arItem["PREVIEW_PICTURE"])): ?>
                    <div class="wow zoomIn" data-wow-duration=".3" data-wow-delay=".1s">
                        <img
                            class="img-responsive"
                            src="<?= SITE_TEMPLATE_PATH ?>/img/no-photo.png"
                        />
                    </div>
                <? endif; ?>
            </div>
            <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                <h3>
                    <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                    <? else: ?>
                        <?= $arItem["NAME"] ?>
                    <? endif; ?>
                    <? if (is_array($arItem["DISPLAY_PROPERTIES"]["SOLUTION"])): ?>
                        <span class="text-uppercase margin-l-20">
                            <?= $arItem["DISPLAY_PROPERTIES"]["SOLUTION"]["DISPLAY_VALUE"] ?>
                        </span>
                    <? endif; ?>
                </h3>

            <? endif; ?>
            <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                <p id="<?= $this->GetEditAreaId($arItem['ID']); ?>"><?= $arItem["PREVIEW_TEXT"]; ?></p>
            <? endif; ?>
            <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                <a class="link"
                   href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= GetMessage('MFT_PRODUCTS_BUTTON') ?></a>
            <? endif; ?>

        </div>
        <? if ($count % 3 == 0 && $count !== 1): ?>
            </div>
        <? endif; ?>
        <? $count++; ?>
    <? endforeach; ?>
</div>
