<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <div class="container">
        <ol class="carousel-indicators">
            <? $count = 0; ?>
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?= $count; ?>"
                    <? if ($count == 0): ?>
                        class="active"
                    <? endif; ?>
                    <? $count++; ?>>
                </li>
            <? endforeach; ?>
        </ol>
    </div>
    <div class="carousel-inner" role="listbox">
        <? $count = 0; ?>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <? $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT")); ?>
            <div class="item<? if ($count == 0) echo ' active'; ?>">
                <? $count = 1; ?>
                <? if (is_array($arItem["PREVIEW_PICTURE"])): ?>
                    <img class="img-responsive" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                         alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>">
                <? else: ?>
                    <img class="img-responsive" src="<?= SITE_TEMPLATE_PATH ?>/img/1920x1080/01.jpg" ">
                <? endif; ?>
                <div class="container" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <div class="carousel-centered">
                        <div class="margin-b-40">
                            <? if ($arItem["NAME"]): ?>
                                <h1 class="carousel-title"><?= $arItem["NAME"] ?></h1>
                            <? endif; ?>
                            <? if ($arItem["PREVIEW_TEXT"]): ?>
                                <p><?= $arItem["PREVIEW_TEXT"]; ?></p>
                            <? endif; ?>
                        </div>
                        <? if (is_array($arItem["PROPERTIES"]["HREF"])): ?>
                            <a href="<?= $arItem["PROPERTIES"]["HREF"]["VALUE"] ?>"
                               class="btn-theme btn-theme-sm btn-white-brd text-uppercase"><?= GetMessage('MFT_SLIDER_BUTTON') ?></a>
                        <? else: ?>
                            <a class="btn-theme btn-theme-sm btn-white-brd text-uppercase"><?= GetMessage('MFT_SLIDER_BUTTON') ?></a>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>