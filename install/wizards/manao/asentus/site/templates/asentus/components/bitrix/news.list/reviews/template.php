<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<div class="content-lg container">
    <div class="row">
        <div class="col-sm-9">
            <h2><?= GetMessage('MFT_REVIEWS_TITLE') ?></h2>
            <div class="swiper-slider swiper-testimonials swiper-container-horizontal">
                <div class="swiper-wrapper"
                     style="transform: translate3d(-1599px, 0px, 0px); transition-duration: 0ms;">
                    <? $count = 1;
                    $flag = false;
                    foreach ($arResult["ITEMS"] as $arItem):
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>

                        <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                             class="swiper-slide <? if ($count == 1) echo ' active' ?>>"
                             data-swiper-slide-index="<?= !(string)$flag ?>" style="width: 533px;">
                            <blockquote class="blockquote">
                                <div class="margin-b-20">
                                    <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                        <?= $arItem["PREVIEW_TEXT"]; ?>
                                    <? endif; ?>
                                </div>
                                <p>
                                    <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                                        <span class="fweight-700 color-link"><?= $arItem["NAME"] ?></span>
                                    <? endif; ?>
                                    <? if ($arItem["DISPLAY_PROPERTIES"]["POSITION"]): ?>
                                        , <?= $arItem["DISPLAY_PROPERTIES"]["POSITION"]["VALUE"]; ?>
                                    <? endif; ?>
                                </p>
                            </blockquote>
                        </div>
                        <? $count = 2;
                        $flag = !$flag; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

