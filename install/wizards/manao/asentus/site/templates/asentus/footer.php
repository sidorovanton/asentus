<footer class="footer">
    <!-- Links -->
    <div class="footer-seperator">
        <div class="content-lg container">
            <div class="row">
                <div class="col-sm-2 sm-margin-b-50">
                    <? $APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
                        "ROOT_MENU_TYPE" => "bottom",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                        false
                    ); ?>
                </div>
                <div class="col-sm-4 sm-margin-b-30">
                    <? $APPLICATION->IncludeFile(
                        SITE_DIR.'include/social_networks_list.php',
                        array(),
                        array('MODE' => 'html')
                    ); ?>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "manao:feedback",
                    "footer",
                    array(
                        "USE_CAPTCHA" => "N",
                        "OK_TEXT" => $APPLICATION->GetProperty("OK_TEXT"),
                        "EMAIL_TO" => $APPLICATION->GetProperty("admins_e_mail"),
                        "REQUIRED_FIELDS" => array(
                            0 => "NAME",
                            1 => "EMAIL",
                            2 => "MESSAGE",
                            3 => "PHONE",
                        ),
                        "AJAX_MODE" => "Y",
                        "EVENT_MESSAGE_ID" => array(),
                        "COMPONENT_TEMPLATE" => "footer"
                    ),
                    false
                ); ?>
            </div>
        </div>
    </div>

    <div class="content container">
        <div class="row">
            <div class="col-xs-6">
                <? $APPLICATION->IncludeFile(
                    SITE_DIR.'include/footer_logo.php',
                    array(),
                    array('MODE' => 'html')
                ); ?>
            </div>
            <div class="col-xs-6 text-right">
                <? $APPLICATION->IncludeFile(
                    SITE_DIR.'include/footer_info.php',
                    array(),
                    array('MODE' => 'html')
                ); ?>
            </div>
        </div>
    </div>
</footer>

<a href="javascript:void(0);" class="js-back-to-top back-to-top">Top</a>

<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- PAGE LEVEL PLUGINS -->
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery.easing.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery.back-to-top.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery.smooth-scroll.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery.wow.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/swiper/js/swiper.jquery.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/masonry/jquery.masonry.pkgd.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/masonry/imagesloaded.pkgd.min.js" type="text/javascript"></script>
<!-- PAGE LEVEL SCRIPTS -->
<script src="<?= SITE_TEMPLATE_PATH ?>/js/layout.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/layout.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/components/wow.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/components/swiper.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/components/masonry.min.js" type="text/javascript"></script>

<script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery.parallax.min.js" type="text/javascript"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/main.js" type="text/javascript"></script>

</body>
<!-- END BODY -->
</html>
