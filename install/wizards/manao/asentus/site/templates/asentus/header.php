<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <? CJSCore::Init(); ?>
    <? CJSCore::Init(array("jquery")); ?>
    <? $APPLICATION->ShowHead() ?>
    <title><?= $APPLICATION->ShowTitle() ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <link href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet" type="text/css">
    <link href="<?= SITE_TEMPLATE_PATH ?>/vendor/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= SITE_TEMPLATE_PATH ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/animate.css" rel="stylesheet">
    <link href="<?= SITE_TEMPLATE_PATH ?>/vendor/swiper/css/swiper.min.css" rel="stylesheet" type="text/css"/>
    <!-- THEME STYLES -->
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/layout.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/img/favicon.ico" type="image/x-icon">
</head>

<body>

<header class="header">
    <? $APPLICATION->ShowPanel() ?>
    <!-- Navbar -->
    <nav class="navbar navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="menu-container">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon"></span>
                </button>
                <!-- Logo -->
                <div class="logo">
                    <? if (CSite::InDir(SITE_DIR . 'index.php')): ?>
                    <a class="logo-wrap">
                        <? else: ?>
                        <a class="logo-wrap" href="<?= SITE_DIR ?>">
                            <? endif; ?>
                            <? $APPLICATION->IncludeFile(
                                SITE_DIR . 'include/header_logo.php',
                                array(),
                                array('MODE' => 'html')
                            ); ?>
                            <? $APPLICATION->IncludeFile(
                                SITE_DIR . 'include/header_logodark.php',
                                array(),
                                array('MODE' => 'html')
                            ); ?>
                        </a>
                </div>
                <!-- End Logo -->
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-collapse">
                <div class="menu-container">

                    <? $APPLICATION->IncludeComponent("bitrix:menu", "header", array(
                        "ROOT_MENU_TYPE" => "top",  //В данном случаее меню типа top
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                        false
                    ); ?>
                </div>
            </div>
            <!-- End Navbar Collapse -->
        </div>
    </nav>
    <!-- Navbar -->
</header>
<? if (!CSite::InDir(SITE_DIR . "index.php")): ?>
    <div class="parallax-window" data-parallax="scroll" data-image-src="<?= SITE_TEMPLATE_PATH ?>/img/1920x1080/01.jpg">
        <div class="parallax-content container">
            <h1 class="carousel-title"><? $APPLICATION->ShowTitle(); ?></h1>
            <p><? $APPLICATION->ShowProperty('description') ?></p>
        </div>
    </div>
<? endif; ?>

