<div class="section-seperator">
	<div class="content-lg container">
		<div class="row">
			<div class="col-sm-4 sm-margin-b-50">
				<div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
					<h3><a href="#">New York</a> <span class="text-uppercase margin-l-20">Head Office</span></h3>
					<p>
						 Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor
					</p>
					<ul class="list-unstyled contact-list">
						<li><i class="margin-r-10 color-base icon-call-out"></i><a href="tel:+101234567890">1 012 3456 7890</a></li>
						<li><i class="margin-r-10 color-base icon-envelope"></i> <a href="mailto:hq@acidus.com">hq@acidus.com</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4 sm-margin-b-50">
				<div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
					<h3><a href="#">London</a> <span class="text-uppercase margin-l-20">Operation</span></h3>
					<p>
						 Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor
					</p>
					<ul class="list-unstyled contact-list">
                                                <li><i class="margin-r-10 color-base icon-call-out"></i><a href="tel:+447734567890">44 77 3456 7890</a></li>
						<li><i class="margin-r-10 color-base icon-envelope"></i> <a href="mailto:operation@acidus.com">operation@acidus.com</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4 sm-margin-b-50">
				<div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
					<h3><a href="#">Singapore</a> <span class="text-uppercase margin-l-20">Finance</span></h3>
					<p>
						 Lorem ipsum dolor sit amet consectetur adipiscing elit sed tempor incdidunt ut laboret dolor magna ut consequat siad esqudiat dolor
					</p>
					<ul class="list-unstyled contact-list">
                                                <li><i class="margin-r-10 color-base icon-call-out"></i><a href="tel:+500124567890">50 012 456 7890</a></li>
						<li><i class="margin-r-10 color-base icon-envelope"></i> <a href="mailto:finance@acidus.com">finance@acidus.com</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>