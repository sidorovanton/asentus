<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (!defined("WIZARD_SITE_ID"))
    return;

if (!defined("WIZARD_SITE_DIR"))
    return;

	$arUrlRewrite = array();
	if (file_exists(WIZARD_SITE_ROOT_PATH."/urlrewrite.php"))
	{
		include(WIZARD_SITE_ROOT_PATH."/urlrewrite.php");
	}

	CUrlRewriter::Add(array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."products/(.+?)/#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => WIZARD_SITE_DIR."products/detail.php",
	));
	CUrlRewriter::Add(array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."services/(.+?)/#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => WIZARD_SITE_DIR."services/index.php",
	));
	CUrlRewriter::Add(array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."products/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => WIZARD_SITE_DIR."products/index.php",
	));	
?>