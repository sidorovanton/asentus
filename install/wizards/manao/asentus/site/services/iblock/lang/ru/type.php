﻿<?
$MESS["ABOUT_COMPANY_TYPE_NAME"] = "О компании";
$MESS["ABOUT_COMPANY_ELEMENT_NAME"] = "О компании";
$MESS["ABOUT_COMPANY_SECTION_NAME"] = "О компании";

$MESS["PRODUCTS_AND_SERVICES_TYPE_NAME"] = "Товары и услуги";
$MESS["PRODUCTS_AND_SERVICES_ELEMENT_NAME"] = "Товары и услуги";
$MESS["PRODUCTS_AND_SERVICES_SECTION_NAME"] = "Товары и услуги";

$MESS["MAIN_PAGE_TYPE_NAME"] = "Главная страница";
$MESS["MAIN_PAGE_ELEMENT_NAME"] = "Главная страница";
$MESS["MAIN_PAGE_SECTION_NAME"] = "Главная страница";
?>