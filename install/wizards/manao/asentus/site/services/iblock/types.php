<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!CModule::IncludeModule("iblock"))
	return;

$arTypes = Array();

$arTypeTemplateMainPage = Array(
		"ID" => "main_page",
		"SECTIONS" => "N",
		"IN_RSS" => "N",
		"SORT" => 50,
		"LANG" => Array(),
);
array_push($arTypes, $arTypeTemplateMainPage);

$arTypeTemplateAboutCompany = Array(
		"ID" => "about_company",
		"SECTIONS" => "N",
		"IN_RSS" => "N",
		"SORT" => 50,
		"LANG" => Array(),
);
array_push($arTypes, $arTypeTemplateAboutCompany);

$arTypeTemplateProductsAndServices = Array(
		"ID" => "products_and_services",
		"SECTIONS" => "N",
		"IN_RSS" => "N",
		"SORT" => 50,
		"LANG" => Array(),
);
array_push($arTypes, $arTypeTemplateProductsAndServices);

$arLanguages = Array();
$rsLanguage = CLanguage::GetList($by, $order, array());
while($arLanguage = $rsLanguage->Fetch())
	$arLanguages[] = $arLanguage["LID"];

foreach($arTypes as $arType)
{
	$iblockType = new CIBlockType;
	$dbType = CIBlockType::GetList(Array(),Array("=ID" => $arType["ID"]));
	if ($dbType->Fetch())
		continue;
	foreach($arLanguages as $languageID)
	{
		WizardServices::IncludeServiceLang("type.php", $languageID);
		$code = strtoupper($arType["ID"]);
		$arType["LANG"][$languageID]["NAME"] = GetMessage($code."_TYPE_NAME");
		$arType["LANG"][$languageID]["ELEMENT_NAME"] = GetMessage($code."MAIN_PAGE_ELEMENT_NAME");
		if ($arType["SECTIONS"] == "Y")
			$arType["LANG"][$languageID]["SECTION_NAME"] = GetMessage($code."_SECTION_NAME");
	}
	$iblockType->Add($arType);
}
?>