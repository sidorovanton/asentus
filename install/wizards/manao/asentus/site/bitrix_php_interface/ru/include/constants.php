<?php
/* IBLOCK IDs */
define("IBLOCK_ID_SERVICES", IBlockData::getByCode('manao_services'));
define("IBLOCK_ID_SLIDER", IBlockData::getByCode('manao_slider'));
define("IBLOCK_ID_PRICING", IBlockData::getByCode('manao_pricing'));
define("IBLOCK_ID_ABOUT", IBlockData::getByCode('manao_about'));
define("IBLOCK_ID_TEAM", IBlockData::getByCode('manao_team'));
define("IBLOCK_ID_REVIEWS", IBlockData::getByCode('manao_reviews'));
define("IBLOCK_ID_FAQ", IBlockData::getByCode('manao_faq'));
define("IBLOCK_ID_PRODUCTS", IBlockData::getByCode('manao_products'));
/* IBLOCK TYPEs */
define("IBLOCK_TYPE_MAIN_PAGE", 'main_page');
define("IBLOCK_TYPE_ABOUT_COMPANY", 'about_company');
define("IBLOCK_TYPE_PRODUCTS_AND_SERVICES", 'products_and_services');
