<?
include ("include/iblockdata.php");
include ("include/constants.php");

AddEventHandler("main", "OnEpilog", "OnEpilogHandler");
function OnEpilogHandler() {
	if(defined('ERROR_404') && ERROR_404 == 'Y') {
		$template = "#TEMPLATE_NAME#";
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.$template.'/header.php';
		include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.$template.'/404.php';
		include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.$template.'/footer.php';
	}
}
?>