<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
 $isAjax = $_REQUEST['ajax'] == 'Y';
?>
<? if (!$isAjax): ?>
<div class="col-sm-5 sm-margin-b-30">
    <? endif; ?>
    <h2 class="color-white"><?= GetMessage("MFT_NOTE") ?></h2>
    <div class="mf-error-text<?=$isAjax ? '-popup' : '';?>">
        <?foreach($arResult['ERROR_MESSAGE'] as $message){
            echo ShowError($message);
        }?>
    </div>
    <?if($arResult['OK_MESSAGE']):?>
        <div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div>
    <?endif;?>
    <form class="user_form" name="user_form" action="<?=$isAjax ? '/' : POST_FORM_ACTION_URI?>" method="POST">
        <?= bitrix_sessid_post() ?>

        <input name="USER_NAME" value="<?= $arResult["AUTHOR_NAME"] ?>" type="text"
               class="form-control footer-input margin-b-20" placeholder="<?= GetMessage("MFT_NAME") ?>" required>
        <input name="USER_EMAIL" type="EMAIL" value="<?= $arResult["AUTHOR_EMAIL"] ?>"
               class="form-control footer-input margin-b-20" placeholder="<?= GetMessage("MFT_EMAIL") ?>" required>
        <input type="text" name="USER_PHONE" value="<?= $arResult["AUTHOR_PHONE"] ?>"
               class="form-control footer-input margin-b-20" placeholder="<?= GetMessage("MFT_PHONE") ?>" required>
        <? if ($isAjax): ?>
            <select name="USER_PRICING" class="form-control margin-b-20 footer-input">
                <? if ($arResult['PRICING_LIST'] && $arResult['PRICING_INDEX']): ?>
                    <? foreach ($arResult['PRICING_LIST'] as $key => $item): ?>
                        <option class="footer-input" <? if ($key == $arResult['PRICING_INDEX']) echo ' selected' ?>>
                            <?= $item ?>
                        </option>
                    <? endforeach; ?>
                <? else: ?>
                    <option>''</option>
                <?endif;?>
            </select>
        <? endif; ?>

        <textarea name="MESSAGE" class="form-control footer-input margin-b-30" rows="6"
                  placeholder="<?= GetMessage("MFT_MESSAGE") ?>"
                  required style="resize: none"><?= $arResult["MESSAGE"] ?></textarea>

        <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
        <input type="submit" class="btn-form-submit btn-theme btn-theme-sm btn-base-bg text-uppercase" name="submit"
               value="<?= GetMessage("MFT_SUBMIT") ?>">
    </form>
    <? if (!$isAjax): ?>
</div>
<? endif;
$param['name']= GetMessage("MFT_ERROR_NAME");
$param['phone']= GetMessage("MFT_ERROR_PHONE");
$param['message']= GetMessage("MFT_ERROR_MESSAGE");
?>
<script>
    $(document).ready(function(){
        popupInit(<?echo json_encode($param); ?>);
    });
</script>

<? if ($isAjax): ?>
    <script>
        // $(".user_form").submit(function() {
        //     return validate(this);
        // });

        var ul = document.getElementsByClassName('mf-error-text-popup');
        var validate = function(el) {
            $(ul).text('');
            $count= 0;
            if(el.user_name.value.length<=2){
                $(ul).append("<p><?= GetMessage("MFT_ERROR_NAME") ?></p>");
                $count++;
            }

            if(!/([\+]+)*[0-9]{9,12}/.test(el.user_phone.value)){
                $(ul).append("<p><?= GetMessage("MFT_ERROR_PHONE") ?></p>");
                $count++;
            }

            if(el.MESSAGE.value.length<10){
                $(ul).append("<p><?= GetMessage("MFT_ERROR_MESSAGE") ?></p>");
                $count++;
            }
            return $count <= 0;
        };
    </script>
<?endif;?>
