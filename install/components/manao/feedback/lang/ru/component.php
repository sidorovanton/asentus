<?
$MESS ['MANAO_MF_OK_MESSAGE'] = "Спасибо, ваше сообщение принято.";
$MESS ['MANAO_MF_REQ_NAME'] = "Укажите ваше имя.";
$MESS ['MANAO_MF_REQ_PHONE'] = "Укажите ваш телефон.";
$MESS ['MANAO_MF_REQ_EMAIL'] = "Укажите E-mail, на который хотите получить ответ.";
$MESS ['MANAO_MF_REQ_MESSAGE'] = "Вы не написали сообщение.";
$MESS ['MANAO_MF_EMAIL_NOT_VALID'] = "Указанный E-mail некорректен.";
$MESS ['MANAO_MF_CAPTCHA_WRONG'] = "Неверно указан код защиты от автоматических сообщений.";
$MESS ['MANAO_MF_CAPTHCA_EMPTY'] = "Не указан код защиты от автоматических сообщений.";
$MESS ['MANAO_MF_SESS_EXP'] = "Ваша сессия истекла. Отправьте сообщение повторно.";
?>
