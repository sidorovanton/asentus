<?
$MESS ['MANAO_MFP_CAPTCHA'] = "Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей";
$MESS ['MANAO_MFP_OK_MESSAGE'] = "Сообщение, выводимое пользователю после отправки";
$MESS ['MANAO_MFP_OK_TEXT'] = "Спасибо, ваше сообщение принято.";
$MESS ['MANAO_MFP_EMAIL_TO'] = "E-mail, на который будет отправлено письмо";
$MESS ['MANAO_MFP_REQUIRED_FIELDS'] = "Обязательные поля для заполнения";
$MESS ['MANAO_MFP_ALL_REQ'] = "(все необязательные)";
$MESS ['MANAO_MFP_NAME'] = "Имя";
$MESS ['MANAO_MFP_MESSAGE'] = "Сообщение";
$MESS ['MANAO_MFP_EMAIL_TEMPLATES'] = "Почтовые шаблоны для отправки письма";
?>
